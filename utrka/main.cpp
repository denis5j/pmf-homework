#include <iostream>
#include <vector>
#include <cstdio>
#include <math.h>
#include <sstream>

using namespace std;

class Plivac{
protected:
    int predena_duljina_ukm;
    int ukupno_vrijeme_plivanja;
    string ime_plivaca;
    virtual string ime()=0;
    virtual int vrijeme()=0;
    virtual int polozaj()=0;
    static vector<Plivac> SviPlivaci;
    static void sviplivaci(){
        for(int i=0;i<SviPlivaci.size();i++){printf("%s",SviPlivaci[i].ime_plivaca.c_str());}
    }
};

vector<Plivac> Plivac::SviPlivaci;

class Tuna:public Plivac{
    int brzina=30;
public:
    Tuna(string ime,int vrijeme){
        ime_plivaca=ime;
        ukupno_vrijeme_plivanja=vrijeme;
        predena_duljina_ukm=vrijeme*brzina;
    }
    string ime(){return ime_plivaca;}
    int vrijeme(){return ukupno_vrijeme_plivanja;}
    int polozaj(){return predena_duljina_ukm;}
};
class Sipa:public Plivac{
    int brzina=20;
public:
    Sipa(string ime,int vrijeme){
        ime_plivaca=ime;
        ukupno_vrijeme_plivanja=vrijeme;
        predena_duljina_ukm=vrijeme*brzina;
    }
    string ime(){return ime_plivaca;}
    int vrijeme(){return ukupno_vrijeme_plivanja;}
    int polozaj(){return predena_duljina_ukm;}
};

void simulirajUtrku(double sati, int br_utrke){
    int broj_parova=ceil(sati/0.5);
    for(int i=0;i<broj_parova;i++){
        ostringstream oss;
        oss << "Tuna-" << i << "-" << br_utrke;
        string ime_tune = oss.str();
        ostringstream oss1;
        oss1 << "Sipa-" << i << "-" << br_utrke;
        string ime_sipe = oss1.str();
        printf("%s\n",ime_sipe.c_str());
        Tuna tuna(ime_tune,sati-0.5*(double)i);
        Sipa sipa(ime_tune,sati-0.5*(double)i);

    }
    printf("%d",broj_parova);
}

int main()
{
    simulirajUtrku(5.2,1);
    return 0;
}
