#include <iostream>
#include <stdio.h>
#include <queue>
#include <list>

struct vatrogasac{
	std::string ime;
	int zvanje;
	int broj_godina;
	vatrogasac(){
		ime="";
		zvanje=0;
		broj_godina=50;
	}
	vatrogasac(std::string _ime){
		ime=_ime;
		zvanje=0;
		broj_godina=50;
	}
	vatrogasac(std::string _ime, int zv, int brG){
		ime=_ime;
		zvanje=zv;
		broj_godina=brG;
	}

};
int dodaj(std::queue<vatrogasac> *a, vatrogasac *b){
        vatrogasac temp;
        bool dodan=false;
        int si=(*a).size(),brojac=0;
        if((*a).size()==0){
            (*a).push((*b));
            return 1;
        }
        for(int i=0;i<si;i++){
            if((*a).front().zvanje==(*b).zvanje){
                brojac++;
                if(2*(9-(*b).zvanje)==brojac)dodan=true;
            }
            if((*a).front().zvanje<(*b).zvanje && !dodan){
                (*a).push((*b));
                dodan=true;
            }
            (*a).push((*a).front());
            (*a).pop();
        }
        if(!dodan)(*a).push((*b));
        return 1;
}
void ispisi(std::queue<vatrogasac> *a){
    int si=(*a).size();
    for(int i=0;i<si;i++){
        std::cout << (*a).front().zvanje;
        (*a).push((*a).front());
        (*a).pop();
    }
    std::cout << std::endl;
}

int dodajPrioritetno(std::queue<vatrogasac> *a, vatrogasac *b){
    std::list<vatrogasac> c;
    std::list<vatrogasac>::iterator li;
    vatrogasac temp;
    int si=(*a).size();
    if((*a).size()==0){
        return 1;
    }
    c.push_back((*b));
    for(int i=0;i<si;i++){
        printf("%d %d\n",(*a).front().zvanje,(*b).zvanje);
        if((*a).front().zvanje==(*b).zvanje){
            for(li=c.begin();li!=c.end();++li){
                    if((*a).front().broj_godina<(*li).broj_godina){
                        c.insert(li,(*a).front());
                     //   (*a).pop();
                        break;
                    }
            }
            if(li==c.end()){
                c.push_back((*a).front());
               // (*a).pop();
            }
        }else{
       //     printf("%d\n",c.size());
           /* if(c.size()!=1){
                for(li=c.begin();li!=c.end();++li){
                    (*a).push((*li));
                }
                c.clear();
            }*/
        }
        (*a).push((*a).front());
        (*a).pop();
    }
    printf("%d\n",c.size());
    return 0;
}
int main(void)
{
    std::queue<vatrogasac> a;
    vatrogasac prvi("ivica",5,50);
    vatrogasac prvi1("ivo",4,30);
    vatrogasac prvi2("denis",1,20);
    vatrogasac prvi3("marko",3,20);
    vatrogasac prvs3("ivo",8,10);
    vatrogasac prvs4("i98vo",8,20);
   // a.push(prvi);
   // a.push(prvi1);
 //   a.push(prvi2);
    dodaj(&a,&prvi3);
    dodaj(&a,&prvi2);
    dodaj(&a,&prvi1);
    dodaj(&a,&prvs3);
    dodaj(&a,&prvs4);
  //  ispisi(&a);
    dodaj(&a,&prvs3);
    ispisi(&a);
    ispisi(&a);
    dodajPrioritetno(&a,&prvs3);

    ispisi(&a);
    int ab(5);
    printf("%d\n",ab);
    //printf("%d\n",a.size());
    return 0;
}
