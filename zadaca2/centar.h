#include <string.h>
#include <iostream>
#include <stdio.h>
#include <utility>
#include <list>
#include <vector>


using namespace std;

class artikl{
public:
    string ime;
    int cijena;
    int trajnost;
    static vector<artikl*> artiklll;
};

vector<artikl*> artikl::artiklll;

class trajni:public artikl{
public:
    trajni(string _ime,int _cijena){
        ime=_ime;
        cijena=_cijena;
        trajnost=20;
        artiklll.push_back(this);
    }
    ~trajni(){
        for(int i=0;i<artiklll.size();i++){
            if(artiklll[i]==this)artiklll.erase(artiklll.begin()+i);
        }
    }
};
class prolazni:public artikl{
public:
    prolazni(string _ime, int _cijena, int _trajnost){
        ime=_ime;
        cijena=_cijena;
        trajnost=_trajnost;
        artiklll.push_back(this);
    }
    ~prolazni(){
        for(int i=0;i<artiklll.size();i++){
            if(artiklll[i]==this)artiklll.erase(artiklll.begin()+i);
        }
    }
};
struct artiklucentru{
    artikl artikll;
    int kolicina;
    int id;
};

class centar{
    list<pair<artikl,int>> artikli;
    static int ukupno_propalo;
    int prop;
    int id;
    public:
        static vector<centar*> svicentri;
        centar(){
            static int _id=0;
            id=_id;
            _id++;
            svicentri.push_back(this);
        }
        int obrisi(string ime, int kolicina);
        bool dobavi(string ime, int koliko);
        pair<int,int> naruci(string ime, int kolicina);
        int propalo();
        int koliko(string ime);
        static void prosaoDan(){
            for(int i=0;i<svicentri.size();i++){
                (*svicentri[i]).prop=0;
                list<pair<artikl,int>>::iterator li;
                for( li = (*svicentri[i]).artikli.begin(); li != (*svicentri[i]).artikli.end(); ++li ){
                    if((*li).second!=0){
                        if((*li).first.trajnost!=20){
                            (*li).first.trajnost--;
                        }
                        if((*li).first.trajnost<0){
                            (*svicentri[i]).prop+=(*li).first.cijena;
                            (*svicentri[i]).artikli.erase(li++);
                        }
                    }
                }
            }
        }
};
int centar::ukupno_propalo=0;
vector<centar*> centar::svicentri;
list<artiklucentru> artikli;
